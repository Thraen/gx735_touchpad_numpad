# gx735_touchpad_numpad

This is a python service which enables switching between numpad and touchpad for the Asus Zephyrus S GX735 series.
It may work for other models.

You need to install the python libevdev package, run install.sh and reboot (or start the service).